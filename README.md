In this assignment a pre-existing project with interfaces was given to us. I was to build an ETL tool that used ORM (Hibernate in this case) to load the data from http://eavesdrop.openstack.org/ into mySQL. I needed to implement the map/transformation of the data before loading it. The data was then available via a GET call to my REST API that I build using RESTEasy.  

-Cesar Rios