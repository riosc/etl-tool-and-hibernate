/*
 * Hibernate, Relational Persistence for Idiomatic Java
 *
 * Copyright (c) 2010, Red Hat Inc. or third-party contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Red Hat Inc.
 *
 * This copyrighted material is made available to anyone wishing to use, modify,
 * copy, or redistribute it subject to the terms and conditions of the GNU
 * Lesser General Public License, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution; if not, write to:
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301  USA
 */
package cs378.assignment6;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Test;

import cs378.assignment6.domain.Project;
import cs378.assignment6.domain.Meeting;

/**
 * Illustrates the use of Hibernate native APIs.  The code here is unchanged from the {@code basic} example, the
 * only difference being the use of annotations to supply the metadata instead of Hibernate mapping files.
 *
 * @author Steve Ebersole
 */
public class AnnotationsIllustrationTest extends TestCase {
	private SessionFactory sessionFactory;

	@Override
	protected void setUp() throws Exception {
		// A SessionFactory is set up once for an application
		sessionFactory = new Configuration()
		.configure() // configures settings from hibernate.cfg.xml
		.buildSessionFactory();
	}

	@Override
	protected void tearDown() throws Exception {
		if ( sessionFactory != null ) {
			sessionFactory.close();
		}
	}

	@SuppressWarnings({ "unchecked" })
	public void testBasicUsage() {
		// create a couple of events...
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save( new Meeting( "Meeting which started the big bang", "2014", "m1"));
		session.save( new Meeting( "A follow up Meeting", "2014", "m1") );
		session.getTransaction().commit();
		session.close();

		// now lets pull events from the database and list them
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Meeting" ).list();
		for ( Meeting event : (List<Meeting>) result ) {
			System.out.println( "Meeting (" + event.getName() + ")");
		}
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings({ "unchecked" })
	public void testMeetingInsert() {
		// create a couple of events...
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save( new Project( "Project 1") );
		session.save( new Project( "Project 2") );
		session.getTransaction().commit();
		session.close();

		// now lets pull events from the database and list them
		session = sessionFactory.openSession();
		session.beginTransaction();
		List result = session.createQuery( "from Project" ).list();
		for ( Project meeting : (List<Project>) result ) {
			System.out.println( "Project (" + meeting.getName() + ") " );
		}
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings({ "unchecked" })
	public void testMeetingEventsInsert() {
		// create a couple of events...
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Project m1 =  new Project( "Project_11");
		session.save(m1);

		Meeting e1 = new Meeting( "Project_11 Meeting 1", "2014", "m1");
		Meeting e2 = new Meeting( "Project_11 Meeting 2", "2014", "m1");

		e1.setProject(m1);
		e2.setProject(m1);

		//m1.addEvent(e1);
		//m1.addEvent(e2);

		session.save(e1);
		session.save(e2);

		Project m2 =  new Project( "Project_21");
		session.save(m2);

		Project m3 =  new Project( "Project_31");
		session.save(m3);

		Project m4 =  new Project( "Project_41");
		session.save(m4);

		Meeting e12 = new Meeting( "Project_21 Meeting 1", "2014", "m1");
		e12.setProject(m2);
		//m2.addEvent(e12);

		session.save(e12);		

		session.getTransaction().commit();
		session.close();

		// now lets pull events from the database and list them
		session = sessionFactory.openSession();
		session.beginTransaction();

		// Select criteria
		Criteria criteria = session.createCriteria(Meeting.class).
				add(Restrictions.eq("name", "Project_11 Meeting 1"));                
		List result = criteria.list();

		for ( Meeting event : (List<Meeting>) result ) {
			System.out.println( "Meeting (" + event.getName() + ") " );
		}

		// Selection criteria
		List<Meeting> events = session.createQuery("from Meeting where project_name='Project_11'").list();
		for ( Meeting event : events ) {
			System.out.println( "Meeting (" + event.getName() + ") " );
		}

		session.getTransaction().commit();
		session.close();

		// Delete -- this will throw ConstraintViolationException
		try { 
			session = sessionFactory.openSession();
			session.beginTransaction();

			Project meetingToDelete = (Project)session.get(Project.class, "Project_21");
			session.delete(meetingToDelete);
			session.getTransaction().commit();
		} catch (ConstraintViolationException e) {        	
			session.getTransaction().rollback();
			session.close();

			session = sessionFactory.openSession();
			session.beginTransaction();

			Project meetingToDelete = (Project)session.get(Project.class, "Project_31");
			session.delete(meetingToDelete);
			session.getTransaction().commit();
			session.close();
		}

		// Parameterized delete --
		session = sessionFactory.openSession();
		session.beginTransaction();

		Query q = session.createQuery("from Project where name = :name ");
		q.setParameter("name", "Project_41");
		Project m = (Project)q.list().get(0);
		session.delete(m);

		session.getTransaction().commit();
		session.close();
	}

	@Test
	public void testError() {
		Client client = ClientBuilder.newClient();
		try {
			Response response = client.target("http://localhost:8080/myeavesdrop/projects/blah/meetings")
					.request().get();

			assertEquals(404, response.getStatus());
		} finally {
			client.close();
		}
	}
}

