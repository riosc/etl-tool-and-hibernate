package cs378.assignment6.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import cs378.assignment6.controller.ETLController;
import cs378.assignment6.etl.Reader;
import cs378.assignment6.service.MeetingDataMgrService;

@Path("/projects")
public class MeetingInfoResource {
	
	private ETLController etlController;
	private Reader meetingDataReader;
	
	public MeetingInfoResource() {
		etlController = new ETLController();
		meetingDataReader = new MeetingDataMgrService();
		
		// Start data load
		(new Thread(etlController)).start();;
	}
	
	// Test method
	@GET
	@Path("/solum/getAll")
	public String getAll() {
		return "Hello, world";
	}
	
	@GET
	@Path("/{project}/meetings")
	@Produces("application/xml")
	public Response getMeetingList(@PathParam("project") String project) {
		if(!project.equals("solum")) {
			StringBuilder xml = new StringBuilder();
			xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xml.append("<name>404</name>");
			return Response.status(400).entity(xml.toString()).build();
		}
		
		String xml = null;
		try {
			xml = (String) meetingDataReader.read("");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Response rs = Response.status(200).entity(xml).build();
		
		return rs;
	}
}
