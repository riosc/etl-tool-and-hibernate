package cs378.assignment6.etl.impl;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.domain.Project;
import cs378.assignment6.etl.Transformer;

public class BasicTransformerImpl implements Transformer {

	public Object transform(Object source) throws Exception {
		Project proj = new Project();
		proj.setName("Solum");
		
		Elements links = ((Document)source).select("a[href*=solum_team_meeting." + 2014 + "]");
		for(Element link: links){
			String str = link.attr("abs:href");
			proj.addMeeting(new Meeting(str.substring(str.indexOf("solum_team_meeting.")), 
					"http://localhost:8080/myeavesdrop/projects/solum/meetings/" + str.substring(str.indexOf("solum_team_meeting.")), 
					"2014", "http://eavesdrop.openstack.org/meetings/solum_team_meeting/2014/"+str.substring(str.indexOf("solum_team_meeting.")), 
							"Solum"));
		}
		
		return proj;
	}
}
