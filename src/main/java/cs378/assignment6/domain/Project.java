/*
 * Hibernate, Relational Persistence for Idiomatic Java
 *
 * Copyright (c) 2010, Red Hat Inc. or third-party contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Red Hat Inc.
 *
 * This copyrighted material is made available to anyone wishing to use, modify,
 * copy, or redistribute it subject to the terms and conditions of the GNU
 * Lesser General Public License, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution; if not, write to:
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301  USA
 */
package cs378.assignment6.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table( name = "PROJECT" )
public class Project {
	
    private String name;
    
//    private String description;
    
    private Set<Meeting> meetingsDesc = new HashSet<Meeting>();

	public Project() {
		// this form used by Hibernate
	}

	public Project(String name) {
		// for application use, to create new events
		this.name = name; 
	}

	@Id
    public String getName() {
		return name;
    }

    public void setName(String name) {
		this.name = name;
    }
	
	public void addMeeting(Meeting meeting) {
		this.meetingsDesc.add(meeting);
	}
	
	@OneToMany(mappedBy="project")
	public Set<Meeting> getMeeting() {
		return meetingsDesc;
	}
	
	public void setMeeting(Set<Meeting> meeting) {
		this.meetingsDesc = meeting;
	}
}