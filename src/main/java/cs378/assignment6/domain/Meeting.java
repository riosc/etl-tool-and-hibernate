package cs378.assignment6.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "MEETINGS" )
public class Meeting {

	private String meeting_name;
	
	private String year;
	
	private String link;
	
	private String description;
	
	private String proj;
	
	private Project project;
	
	public Meeting() {
	}
	
	public Meeting(String name, String year, String link, Project project) {
		this.meeting_name = name;
		this.year = year;
		this.link = link;
		this.project = project;
	}
	
	public Meeting(String name, String link, String year, String desc, String proj) {
		this.meeting_name = name;
		this.year = year;
		this.link = link;
		this.description = desc;
		this.proj = proj;
	}
	
	@Id	
	public String getName() {
		return this.meeting_name;
	}
	
	public void setName(String name) {
		this.meeting_name = name;
	}
	
	@Column(name = "link")
	public String getLink() {
		return this.link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	@Column(name = "year")
	public String getYear() {
		return this.year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Column(name = "proj")
	public String getProj() {
		return this.proj;
	}
	
	public void setProj(String proj) {
		this.proj = proj;
	}

	public void setProject(Project project) {
		this.project = project;
	}
		
	@ManyToOne
	@JoinColumn(name="project_name")
	public Project getProject() {
		return this.project;
	}
}