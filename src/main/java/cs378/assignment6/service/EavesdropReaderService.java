package cs378.assignment6.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import cs378.assignment6.etl.Reader;

public class EavesdropReaderService implements Reader {

	public Object read(Object source) throws Exception {
		Document doc = Jsoup.connect((String)source).get();
		return doc;
	}
}
