package cs378.assignment6.service;

import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import cs378.assignment6.domain.Meeting;
import cs378.assignment6.domain.Project;
import cs378.assignment6.etl.Loader;
import cs378.assignment6.etl.Reader;

public class MeetingDataMgrService implements Loader, Reader {
	private SessionFactory sessionFactory;
	
	public MeetingDataMgrService() {
		// A SessionFactory is set up once for an application
        sessionFactory = new Configuration()
                .configure() // configures settings from hibernate.cfg.xml
                .buildSessionFactory();
	}
	
	public void load(Object objectToLoad) throws Exception {
		insertMeetingRecord(objectToLoad);
	}
	
	private void insertMeetingRecord(Object obj) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Project proj = (Project)obj;
		
		Iterator iter = proj.getMeeting().iterator();
		while (iter.hasNext()) {
			session.save(iter.next());
		}
		
		session.save(proj);
		
		session.getTransaction().commit();
		session.close();
	}

	public Object read(Object source) throws Exception {

		// Build the list of meetings
		
		// Use Hibernate to query meeting list from the table
		// Build MeetingList and send it back
		
		StringBuffer xml = new StringBuffer();
		xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		xml.append("<project>");
		xml.append("<name>Solum</name>");
		xml.append("<meetings>");

		   /* Method to  READ all the employees */
		      Session session = sessionFactory.openSession();
		      Transaction tx = null;
		      try{
		         tx = session.beginTransaction();
		         List employees = session.createQuery("FROM Meeting").list(); 
		         for (Iterator iterator = 
		                           employees.iterator(); iterator.hasNext();){
		            Meeting meeting = (Meeting) iterator.next(); 
		            xml.append("<meeting>");
		            xml.append("<name>" + meeting.getName() + "</name>");
		            xml.append("<year>" + meeting.getLink() + "</year>");
		            xml.append("<link>" + meeting.getYear() + "</link>");
		            xml.append("<description>" + "http://eavesdrop.openstack.org/meetings/solum_team_meeting/2014/"+meeting.getName() + "</description>");
		            xml.append("</meeting>");
		         }
		         tx.commit();
		      }catch (HibernateException e) {
		         if (tx!=null) tx.rollback();
		         e.printStackTrace(); 
		      }finally {
		         session.close(); 
		      }
	      
	      xml.append("</meetings>");
	      xml.append("</project>");
		return xml.toString();
	}
}
